full_signal=$(iwconfig 2>/dev/null | grep -oP '(?<=Signal level=).*')
signal=$(echo "$full_signal" | awk '{print $1}')

if (($signal<=0 && $signal>=-30))
then
	echo "<txt><span weight='Bold' fgcolor='Blue'>$full_signal</span></txt>"
elif (($signal<=-31 && $signal>=-67))
then
	echo "<txt><span weight='Bold' fgcolor='Green'>$full_signal</span></txt>"
elif (($signal<=-68 && $signal>=-70))
then
	echo "<txt><span weight='Bold' fgcolor='Yellow'>$full_signal</span></txt>"
elif (($signal<=-71 && $signal>=-80))
then
	echo "<txt><span weight='Bold' fgcolor='Orange'>$full_signal</span></txt>"
elif (($signal<=-81 && $signal>=-90))
then
	echo "<txt><span weight='Bold' fgcolor='Red'>$full_signal</span></txt>"
else
	echo "$full_signal"
fi
