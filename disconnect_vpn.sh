#!/bin/bash
# quick disconnect for PIA with nm
nmcli connection down "$(nmcli -t connection show --active | grep vpn | awk -F":" '{print $1}')"
