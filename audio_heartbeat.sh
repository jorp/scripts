#!/bin/bash
# check if PulseAudio sound server running
# fake keep alive screen with simulate key press
# this was mainly used up until xfce4-screensaver 0.1.8
# ability to inhibit locking on fullscreen apps/video

while :
do
	# alsa:
	# if [[ ! -z $(grep RUNNING /proc/asound/card*/pcm*/sub*/status) ]] ; then
	if [[ ! -z $(pacmd list-sink-inputs | grep RUNNING) ]] ; then
		nice -n 1 xdotool key shift ;
	fi
	sleep 100
done
